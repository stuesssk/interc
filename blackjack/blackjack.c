
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "card.h"
#include "deck.h"
#include "hand.h"

int main(void)
{
	srand(time(NULL));

	struct deck *pile = deck_create(1);

	struct card **hand = deck_deal(pile, 5);
	hand_print(hand);
	printf("%d\n", blackjack_score(hand));

	free(hand);
	deck_destroy(pile);
}
