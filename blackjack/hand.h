
#ifndef HAND_H
 #define HAND_H

#include "card.h"

void hand_print(struct card *hand[]);

int blackjack_score(const struct card * const hand[]);

int blackjack_cmp(const struct card *const a[], const struct card *const b[]);

#endif
