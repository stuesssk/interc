
#include <stdio.h>
#include <sysexits.h>

int main(int argc, char *argv[])
{
	if(argc != 2) {
		fprintf(stderr, "Usage: %s <file>\n", argv[0]);
		return EX_NOINPUT;
	}

	FILE *fh = fopen(argv[1], "r");
	if(!fh) {
		perror("Unable to open file");
		return EX_NOINPUT;
	}

}
