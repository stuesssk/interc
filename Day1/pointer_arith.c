
#include <stdio.h>

void find_negs(double *start, double *limit);

int main(void)
{
	double input[] = { 8.1, -7, 0, -3.1, 87, 9.12, 87.5, 4.4, -1000 };
  double *end;
  end = input + 9;

	find_negs(input, end);
}

void find_negs(double *start, double *limit)
{
  if(!start){
    return;
  }

	while (start < limit){
		if(*start < 0) {
			printf("Found %lf\n", *start);
		}
    ++start;
	}
}
