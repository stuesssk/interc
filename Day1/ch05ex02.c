#include <stdio.h>

void sort3(int *ptr1, int *ptr2, int *ptr3);

int main(void)
{
    int a = 7, b = 4, c = 2;
    sort3(&a, &b, &c);
    printf("After: a = %d, b = %d, c = % d\n", a, b, c);

}


void sort3(int *ptr1, int *ptr2, int *ptr3)
{
    while ( *ptr1 > *ptr2 || *ptr2 > *ptr3){
        int buff;
        if (*ptr1 > *ptr2){
            buff = *ptr1;
            *ptr1 = *ptr2;
            *ptr2 = buff;
        }

        if (*ptr2 > *ptr3){
            buff = *ptr2;
            *ptr2 = *ptr3;
            *ptr3 = buff;
        }
    }  



}
