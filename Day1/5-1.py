#!/usr/bin/python3

import sys

def main():
    filename = sys.argv[1]

    words = []
    count = []

    with open(filename) as fh:
        for line in fh:
            for word in line.split():
                for i, alread_seen in enumerate(words):
                    if word == alread_seen:
                        count[i] += 1
                        break
                else:
                    words.append(word)
                    count.append(1)


    for i in range(len(words)):
        print("{}: {}".format(words[i], count[i]))


if __name__ == '__main__':
    main()
