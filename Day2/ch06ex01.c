#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[])
{


    if (argc != 4 ){
        fprintf(stderr,"Usage: %s <file1> <value> <value> <value>\n", argv[0]);
        return 1;
    }


    int start = strtol(argv[1],NULL,10 );
    int stop = strtol(argv[2],NULL,10 );
    int step = strtol(argv[3],NULL,10 );
    printf("Celsius      Fahrenheit\n");
    for(int i = start; i <= stop; i += step){
        double fahrn = 1.8*i + 32;
        printf("%-2d           %.1lf\n",i,fahrn);
    }
}
