#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

bool in_array(int *haystack, size_t sz, int needle);

int main()
{
    int haystack[10] = {1,2,3,4,5,6,7,8,9,10};
    size_t sz = sizeof(haystack);
    int needle;
    char buf[16];
    printf("Input a number: ");
	needle = strtol(fgets(buf, sizeof(buf), stdin),NULL,10);
    if (in_array(haystack, sz, needle)){
        printf("You found the needle in the haystack\n");
    }
    printf("The Needel is not in the haystack\n");

}



bool in_array(int *haystack, size_t sz, int needle)
{
	for(size_t i = 0; i < sz ; ++i){
        if(haystack[i] == needle){
            return true;
        }
	}
    return false;
}
