#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int lookup(char **argv, int count, char *str);

int main (int argc, char *argv[])
{
    char buf[128];
    printf("Input a string: ");
	fgets(buf, sizeof(buf), stdin);

    buf[strlen(buf)-1] = '\0';

    int position = lookup(argv, argc , buf );
    if( position >= 0){
        printf("%s was argument number %d\n", buf, position);
    }else{
        printf("%s was not an argument.\n",buf);
    }

}

int lookup(char **argv, int count, char *str)
{
    for( int i = 0; i < count; ++i){
        if (!(strncmp(argv[i], str, strlen(argv[i])))){
            return i;
        }
    }
    return -1;
        

}
