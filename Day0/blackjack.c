
#include <stdio.h>

#include "card.h"
#include "deck.h"

int main(void)
{
	struct deck pile = deck_create();
	char buf[18];

	card_format(buf, sizeof(buf), pile.cards[17]);
	puts(buf);

	printf("card: %zd\n", sizeof(struct card));
	printf("deck: %zd\n", sizeof(struct deck));
}
