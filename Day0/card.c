
#include "card.h"

#include <stdio.h>
#include <string.h>

bool card_format(char output[], size_t sz, struct card c)
{
	if(sz < 18) {
		return false;
	}

	snprintf(output, sz, "%d of %d", c.rank, c.suit);

	return true;
}
