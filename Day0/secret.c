
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void secret_number(int *secret)
{
  // seeding RNG with clock time
  srand(time(NULL));

  // Creating a random secret number and storing each digit in the array
  for(int i = 0; i<4 ; ++i){
    secret[i] = rand()%10;
  }
}
