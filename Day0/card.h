#ifndef CARD_H
#define CARD_H

#include <stdbool.h>
#include <stdlib.h>

enum suit_name {
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES
};

struct card {
	int rank;
	enum suit_name suit;
};


bool card_format(char output[], size_t sz, struct card c);

#endif
