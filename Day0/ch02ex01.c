#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "ch02ex01.h"

void user_guess(int *guess);
void secret_number(int *secret);
int guess_check (int *guess, int *secret, int *turn_counter);


int main(int argc, char *argv[])
{
  int turn_counter = 0;
  int secret[4];
  
  // Using argc to determine where secret number comes from
  // argc = 1 no user input and pick random. argc = 2 read from file.mm to
  // get number.
  if (argc == 1){
    secret_number(secret);

  // with 2 cmd line arguments argc[1] is file with target number
  // if any errors in retrieving or reading the target from file
  // will just default to picking a random number.
  }else if (argc == 2){
    // ensuring argument supplied has a .mm file extension
    char test[32];
    strncpy(test, argv[1], 16);
    int length = strlen(test);
    char file[3]={test[length-3], test[length-2], test[length-1]};
    char comp[3] = ".mm";
    // testing the last three char of the file supplied in cmd line to 
    // see if it is a .mm file
    if (file[0] != comp[0] || file[1] != comp[1] || file[2] != comp[2]){
      printf("File supplied is not a .mm file, defaulting to random secret\n");
      secret_number(secret);
      goto play_game;
    }

    // opening file, if unable defaulting to random secret number
    FILE *fp = fopen(argv[1], "r");
    if (!fp){
      printf("Unable to open file 1 for reading, defualting to random secret.\n");
      secret_number(secret);
      goto play_game;
    }

    char buf[128];
    fgets(buf, sizeof(buf), fp);
    // If string is to long default to random secret
    if (strlen(buf) > 5){
      printf("File target is to large, defaulting to random secret.\n");
      secret_number(secret);
    //  Made it this far you can use the target in the file as long as it is all digits
    }else{
      strtol(buf,NULL,10);
      for(int i = 0; i < 4 ; ++i){
        // ensure all parts of target number are digits and if not default to random
        if (isdigit(buf[i])){
          secret[i] = buf[i] - '0';
          // string conversion to int taken from and storage into secret array
          // stackoverflow.com/questions/19468556/converting-char-array-to-int-arry
          // '0' subtracts ASCII value from the char leaving only the integers 0-9.
          // isdigit and digit monitoring ensure only numbers 0-9 will make it here.
        }else{
          printf("Target value contains a non-numeric value. Defaulting to random secret.\n");
          secret_number(secret);
        }
      }
    }
    fclose(fp);

  // If more than 1 other cmd line argument just default to random secret
  }else{
      fprintf(stderr, "Usage: %s <file1> <file2>\n", argv[0]);
      printf("Defaulting to random secret.\n");
      secret_number(secret);
  }

play_game:
// Enter the guessing loop. All who enter do not leave until the 
// secret number has been found.
  while(1){
    int guess[4];
    // Jump to function to gather users guess
    user_guess(guess);

    // Test users guess returns 1 when win, all other times returns 0
    int test = guess_check(guess, secret, &turn_counter);
    // If test is 1 you won and break out of guessing loop
    if (test){
      break;
    }
  }
  printf("You win! It took you %d guessess\n", turn_counter);
}
