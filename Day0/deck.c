
#include "deck.h"

#include <string.h>

struct deck deck_create(void)
{
	struct deck d;
	for(int s = 0; s < 4; ++s) {
		for(int rank = 2; rank < 15; ++rank) {
			size_t i = s*13 + (rank - 2);

			d.cards[i].suit = s;
			d.cards[i].rank = rank;
		}
	}

	return d;
}
