#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void user_guess(int *guess)
{
  char temp[128];

guess:

  // Capture users 4 digit guess
  printf("Guess a four digit number: ");
  scanf("%s", temp);

  // Error handle for an input longer than 4 characters
  if (isprint(temp[4])){
    printf("You enteered a number with more than four characters,\n");
    goto guess;
  }

  // Users number is put into an array
  for(int i = 0; i < 4 ; ++i){
    if (!isdigit(temp[i])){
      printf("You entered a Non-Digit or under 4 digits.\n");
      goto guess;
    }
    // string conversion to int taken from
    // stackoverflow.com/questions/19468556/converting-char-array-to-int-arry
    // '0' subtracts ASCII value from the char leaving only the integers 0-9.
    // isdigit and digit monitoring ensure only numbers 0-9 will make it here.
    guess[i] = temp[i]- '0';
  }
}

