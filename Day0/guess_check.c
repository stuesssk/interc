#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int guess_check (int *guess, int *secret,int *turn_counter)
{
  // int check_guess[4] = {1,1,1,1} & int check_guess[4] = {1,1,1,1}
  // used to keep track of what digits in the secret have beeen matched with
  // users guess for each itteration of checking. When matched the corresponding
  // index in the arrays get turned to 0 so they canbe skipped over in the following
  // iterations of this guess. Both arrays are reset at the start of each guess check
  int check_secret[4] = {1,1,1,1};
  int check_guess[4] = {1,1,1,1};
  int red_count = 0;
  int white_count = 0;

  *turn_counter += 1;

  // checking for the reds
  for (int i = 0; i < 4; ++i){
    if (guess[i] == secret[i]){
      ++red_count;
      //  if they are equal they don't need to be compared in the white
      check_secret[i] = check_guess[i] =0;
      // match all 4 you win!!!
      if (red_count == 4){
        return 1;
      }
    }
  }
  // When no red matches, omit red print statement
  if (red_count > 0){
    printf("%d Red",red_count);
  }

  // checking for the whites
  for(int i = 0; i < 4; ++i){
    for(int j = 0; j < 4; ++j){
      if (guess[i] == secret[j] && check_secret[j] && check_guess[i] && i != j){
        ++white_count;
        // Blocks digit in secret being double counted for white
        check_secret[j] = check_guess[i] = 0;
        break;
      }
    }
  }

  // Print comma only when printing both white and red results
  if (red_count > 0 && white_count > 0){
    printf(", ");
  }
  if (white_count > 0){
    printf("%d White",white_count);
  }
  if (white_count == 0 && red_count == 0){
    printf("No Matches");
  }
  printf("\n");
  return 0;
}
